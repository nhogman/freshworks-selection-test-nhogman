# freshworks-selection-test-nhogman

	Application can be found at: https://tranquil-oasis-52624.herokuapp.com/

	My approach to the problem:
		- started out by reading over the task and making a list of what the requirements ask. In this case what data the phD student is looking to gather.
			
		High	1. As a User i would like to be able to enter data about the feeding of ducks that contains:
					a. What time the ducks are fed
					b. What food the ducks are fed
					c. Where the ducks are fed
					d. How many ducks that were fed
					e. What kind of food the ducks were fed (refer to point b.)
					f. How much food the ducks were fed
		Low		2. As a User i would like to be able to set up a repeating scheduled feeding


		- once i had that i started to draw up a quick plan for a database model. This went through a few iterations and the earlier ones were a bit to big and complicated for the task at hand.
		
		- next i built the database and started work of duplication the data model into the ORM. With that i build the migration plan to instantiate a new database with some starting values that could be used for some of the tables
		
		- after that the api was the next thing to make. going over everything the front end would need i created the API that the client side would need. Since there was no requirements for updating any data i left it to just creating and reading.
		
		- Once the api was complete i did some naive testing using postman.
		
		- Next i set up the client side environment for react and started making a bare bones app to handle the creation of the feeding.
		
		- After i had a bare bones app, i sent out to look into hosting the application. This is an area i haven't had too much experience in and ended up spending the majority of he time doing this work. Though i have learned a fair bit from just doing this application.
		
		- Lastly i wrote some unit tests for the models and got the testing framework started

		- would have hope to get some styling and more testing done, but a lot of time was eaten up by hosting problems


	Technologies chosen:
		- Postgres (database)
			This was chosen as i have the most experience working with postgres, as well having completed the db admin requirements
		- Sequelize (ORM)
			I have used it once before on a home project and found it simple and light weight, but still gives me the comfort of feeling like writing a java dal. Would probaby prefer to use mongoose if i was to do this over.
		- React (front-end library)
			current library i have been using and home and work. Decided to learn this over angular as it allows for more custimization with using other technologies. Though i did leave this to the end of my time i had and did not get to spend much time in this area on this task.
		- Express (node.js API)
			easy quick way to get an api up and running in node.
		- Horaku (Hosting Platform)
			AWS was giving me some trouble and this one provided me with a lot better path to setting up a node.js app


	A high-level component diagram:
		- see attached image

	A database model diagram:
		- see attached image

	Roughly how many hours spent:
		- I spent about 9 hours on this app, with a majority of the time spent hosting the application.


