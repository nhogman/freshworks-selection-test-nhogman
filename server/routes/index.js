const controllers = require('../controllers');
const feedingController = controllers.feeding;
const countryController = controllers.country;
const cityController = controllers.city;
const parkController = controllers.park;
const foodTypeController = controllers.foodType;
const quantityTypeController = controllers.quantityType;

module.exports = (app) => {
	app.get('/api', (req, res) => res.status(200).send({
		message: 'Welcome to feeding API!',
	}));

	/* 
		POST
	*/
	/* 
		@params {
			name,
			countryCode,
		}
		returns Country
	*/
	app.post('/api/countries', countryController.create);
	/* 
		@params {
			name,
			countryId
		}
		returns City
	*/
	app.post('/api/countries/:countryId/cities', cityController.create);
	/* 
		@params {
			name,
			cityId,
		}
		returns Park
	*/
	app.post('/api/cities/:cityId/parks', parkController.create);
	/* 
		@params {
			parkId,
			foodTypeId,
			quantity,
			quantityTypeId,
			numberOfDucks,
			feedingTime,
			
		}
		returns Feeding
	*/
	app.post('/api/parks/:parkId/feedings', feedingController.create);
	/* 
		@params {
			name,
			description,
		}
		returns FoodType
	*/
	app.post('/api/foodTypes', foodTypeController.create);

	/* 
		GET
	*/ 
	/* 
		returns Array<Country>
	*/ 
	app.get('/api/countries', countryController.list);
	/* 
		@params countyId
		returns Country
	*/ 
	app.get('/api/countries/:countryId', countryController.findById);
	/* 
		@params countyId
		returns Array<City>
	*/ 
	app.get('/api/countries/:countryId/cities', cityController.findByCountryId);
	/* 
		@params cityId
		returns City
	*/ 
	app.get('/api/cities/:cityId', cityController.findById);
	/* 
		@params cityId
		returns Array<Park>
	*/ 
	app.get('/api/cities/:cityId/parks', parkController.findByCityId);
	/* 
		@params parkId
		returns Park
	*/ 
	app.get('/api/parks/:parkId', parkController.findById);
	/* 
		returns Array<FoodType>
	*/ 
	app.get('/api/foodTypes', foodTypeController.list);
	/* 
		@params foodTypesId
		returns FoodType
	*/ 
	app.get('/api/foodTypes/:foodTypesId', foodTypeController.findById);
	/* 
		returns Array<QuantityType>
	*/ 
	app.get('/api/quantityTypes', quantityTypeController.list);
	/* 
		@params quantityTypeId
		returns QuantityType
	*/ 
	app.get('/api/quantityTypes/:quantityTypeId', quantityTypeController.findById);
	/* 
		returns Array<Feeding>
	*/ 
	app.get('/api/feedings', feedingController.list);

	
};