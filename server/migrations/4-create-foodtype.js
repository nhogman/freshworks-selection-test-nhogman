const currentTime = new Date();

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FoodTypes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      description: {
        type: Sequelize.STRING(500),
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(function () {
      queryInterface.bulkInsert('FoodTypes',
        [{
          name: 'seeds',
          description: 'generic mixed bird seeds.',
          createdAt: currentTime,
          updatedAt: currentTime,
        },{
          name: 'bread',
          description: 'generic bread.',
          createdAt: currentTime,
          updatedAt: currentTime,
        },
        {
          name: 'corn',
          description: 'generic corn.',
          createdAt: currentTime,
          updatedAt: currentTime,
        },
        {
          name: 'duck pellets',
          description: 'generic duck pellets.',
          createdAt: currentTime,
          updatedAt: currentTime,
        },
        {
          name: 'lettuce/greens',
          description: 'generic lettuce and other greens.',
          createdAt: currentTime,
          updatedAt: currentTime,
        },
        {
          name: 'frozen peas',
          description: 'generic frozen peas.',
          createdAt: currentTime,
          updatedAt: currentTime,
        },
        ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('FoodTypes');
  }
};