const currentTime = new Date();

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Countries', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      countryCode: {
        type: Sequelize.STRING(3),
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
    .then(function () {
      queryInterface.bulkInsert('Countries',
        [{
          name: 'Canada',
          countryCode: 'CAN',
          createdAt: currentTime,
          updatedAt: currentTime,
        },{
          name: 'United States of America',
          countryCode: 'USA',
          createdAt: currentTime,
          updatedAt: currentTime,
        },{
          name: 'England',
          countryCode: 'ENG',
          createdAt: currentTime,
          updatedAt: currentTime,
        },{
          name: 'France',
          countryCode: 'FRN',
          createdAt: currentTime,
          updatedAt: currentTime,
        },{
          name: 'Germany',
          countryCode: 'GER',
          createdAt: currentTime,
          updatedAt: currentTime,
        },{
          name: 'Peru',
          countryCode: 'PER',
          createdAt: currentTime,
          updatedAt: currentTime,
        },{
          name: 'Ghana',
          countryCode: 'GHA',
          createdAt: currentTime,
          updatedAt: currentTime,
        },{
          name: 'Russia',
          countryCode: 'RUS',
          createdAt: currentTime,
          updatedAt: currentTime,
        }]
      );
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Countries');
  }
};