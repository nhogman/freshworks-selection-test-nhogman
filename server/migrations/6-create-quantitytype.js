const currentTime = new Date();

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('QuantityTypes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      unit: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      description: {
        type: Sequelize.STRING(500),
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
    .then(function () {
      queryInterface.bulkInsert('QuantityTypes',
        [{
          unit: 'pounds',
          description: 'Imperial pounds.',
          createdAt: currentTime,
          updatedAt: currentTime,
        },{
          unit: 'kilogram',
          description: 'Metric Kilogram.',
          createdAt: currentTime,
          updatedAt: currentTime,
        },
        {
          unit: 'gram',
          description: 'metric grams.',
          createdAt: currentTime,
          updatedAt: currentTime,
        },
        {
          unit: 'ounce',
          description: 'Imperial ounce.',
          createdAt: currentTime,
          updatedAt: currentTime,
        },
        ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('QuantityTypes');
  }
};