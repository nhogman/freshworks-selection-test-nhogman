module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Feedings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      feedingTime: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      quantity: {
        type: Sequelize.DOUBLE,
        allowNull: false,
      },
      numberOfDucks: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      parkId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Parks',
          key: 'id',
          as: 'parkId',
        },
        allowNull: false,
      },
      foodTypeId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'FoodTypes',
          key: 'id',
          as: 'foodTypeId',
        },
        allowNull: false,
      },
      quantityTypeId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'QuantityTypes',
          key: 'id',
          as: 'quantityTypeid',
        },
        allowNull: false,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Feedings');
  }
};