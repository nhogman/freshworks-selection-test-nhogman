module.exports = (sequelize, DataTypes) => {
  const Country = sequelize.define('Country', {
    countryCode: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {});
  Country.associate = (models) => {
    // associations can be defined here
  };
  return Country;
};