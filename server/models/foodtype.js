module.exports = (sequelize, DataTypes) => {
  const FoodType = sequelize.define('FoodType', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING,
    },
  }, {});
  FoodType.associate = (models) => {
    // associations can be defined here
  };
  return FoodType;
};