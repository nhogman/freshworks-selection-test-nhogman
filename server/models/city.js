module.exports = (sequelize, DataTypes) => {
  const City = sequelize.define('City', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {});
  City.associate = (models) => {
    // associations can be defined here
    City.belongsTo(models.Country, {
      foreignKey: 'countryId',
      onDelete: 'CASCADE',
    });
  };
  return City;
};