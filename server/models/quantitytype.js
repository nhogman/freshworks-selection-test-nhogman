module.exports = (sequelize, DataTypes) => {
  const QuantityType = sequelize.define('QuantityType', {
    unit: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING,
    }
  }, {});
  QuantityType.associate = (models) => {
    // associations can be defined here
  };
  return QuantityType;
};