module.exports = (sequelize, DataTypes) => {
  const Feeding = sequelize.define('Feeding', {
    quantity: {
      type: DataTypes.DOUBLE,
      allowNull: false,
    },
    numberOfDucks: {
    	type: DataTypes.INTEGER,
    	allowNull: false,
    },
    feedingTime: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  }, {});
  Feeding.associate = (models) => {
    Feeding.belongsTo(models.Park, {
      foreignKey: 'parkId',
      onDelete: 'CASCADE',
    });
    Feeding.belongsTo(models.FoodType, {
      foreignKey: 'foodTypeId',
      onDelete: 'CASCADE',
    });
    Feeding.belongsTo(models.QuantityType, {
      foreignKey: 'quantityTypeId',
      onDelete: 'CASCADE',
    });
  };
  return Feeding;
};
