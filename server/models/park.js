module.exports = (sequelize, DataTypes) => {
  const Park = sequelize.define('Park', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {});
  Park.associate = (models) => {
    // associations can be defined here
    Park.belongsTo(models.City, {
      foreignKey: 'cityId',
      onDelete: 'CASCADE',
    });
  };
  return Park;
};