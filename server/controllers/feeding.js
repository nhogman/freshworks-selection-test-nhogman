const Feeding = require('../models').Feeding;

module.exports = {
	create(req, res) {
		return Feeding
			.create({
				parkId: req.params.parkId,
				foodTypeId: req.body.foodTypeId,
				quantity: req.body.quantity,
				quantityTypeId: req.body.quantityTypeId,
				numberOfDucks: req.body.numberOfDucks,
				feedingTime: req.body.feedingTime,
			})
			.then(feeding => res.status(201).send(feeding))
			.catch(error => res.status(400).send(error));
	},
	list(req, res) {
		return Feeding
			.all()
			.then(feedings => res.status(200).send(feedings))
			.catch(error => res.status(400).send(error));	
	},
};