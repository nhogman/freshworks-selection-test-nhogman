const QuantityType = require('../models').QuantityType;

module.exports = {
	list(req, res) {
		return QuantityType
			.all()
			.then(quantityTypes => res.status(200).send(quantityTypes))
			.catch(error => res.status(400).send(error));	
	},
	findById(req, res) {
		return QuantityType
			.findById(req.params.quantityTypeId)
			.then(quantityType => {
				if(!quantityType) {
					return res.status(404).send({
						message: 'Quantity Type not found',
					});
				}
				return res.status(200).send(quantityType)
			})
			.catch(error => res.status(400).send(error));
	},
};