const City = require('../models').City;

module.exports = {
	create(req, res) {
		return City
			.create({
				name: req.body.name,
				countryId: req.params.countryId,
			})
			.then(city => res.status(201).send(city))
			.catch(error => res.status(400).send(error));
	},
	list(req, res) {
		return City
			.all()
			.then(cities => res.status(200).send(cities))
			.catch(error => res.status(400).send(error));	
	},
	findById(req, res) {
		return City
			.findById(req.params.cityId)
			.then(city => {
				if(!city) {
					return res.status(404).send({
						message: 'City not found',
					});
				}
				return res.status(200).send(city)
			})
			.catch(error => res.status(400).send(error));
	},
	findByCountryId(req, res) {
		return City
			.findAll({
				where: {
					countryId: req.params.countryId,
				}
			})
			.then(cities => res.status(200).send(cities))
			.catch(error => res.status(400).send(error));
	},
};