const FoodType = require('../models').FoodType;

module.exports = {
	create(req, res) {
		return FoodType
			.create({
				name: req.body.name,
				description: req.body.description,
			})
			.then(foodType => res.status(201).send(foodType))
			.catch(error => res.status(400).send(error));
	},
	list(req, res) {
		return FoodType
			.all()
			.then(foodTypes => res.status(200).send(foodTypes))
			.catch(error => res.status(400).send(error));	
	},
	findById(req, res) {
		return FoodType
			.findById(req.params.foodTypeId)
			.then(foodType => {
				if(!foodType) {
					return res.status(404).send({
						message: 'Food type not found',
					});
				}
				return res.status(200).send(foodType)
			})
			.catch(error => res.status(400).send(error));
	},
};