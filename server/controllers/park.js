const Park = require('../models').Park;

module.exports = {
	create(req, res) {
		return Park
			.create({
				name: req.body.name,
				cityId: req.params.cityId,
			})
			.then(park => res.status(201).send(park))
			.catch(error => res.status(400).send(error));
	},
	list(req, res) {
		return Park
			.all()
			.then(parks => res.status(200).send(parks))
			.catch(error => res.status(400).send(error));	
	},
	findById(req, res) {
		return Park
			.findById(req.params.parkId)
			.then(park => {
				if(!park) {
					return res.status(404).send({
						message: 'Park not found',
					});
				}
				return res.status(200).send(park)
			})
			.catch(error => res.status(400).send(error));
	},
	findByCityId(req, res) {
		return Park
			.findAll({
				where: {
					cityId: req.params.cityId,
				}
			})
			.then(parks => res.status(200).send(parks))
			.catch(error => res.status(400).send(error));
	},
};