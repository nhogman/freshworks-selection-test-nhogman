const feeding = require('./feeding');
const country = require('./country');
const city = require('./city');
const park = require('./park');
const foodType = require('./foodType');
const quantityType = require('./quantityType');

module.exports = {
	feeding,
	city,
	park,
	foodType,
	country,
	quantityType,
};