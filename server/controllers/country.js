const Country = require('../models').Country;

module.exports = {
	create(req, res) {
		return Country
			.create({
				name: req.body.name,
				countryCode: req.body.countryCode,
			})
			.then(country => res.status(201).send(country))
			.catch(error => res.status(400).send(error));
	},
	list(req, res) {
		return Country
			.all()
			.then(countries => res.status(200).send(countries))
			.catch(error => res.status(400).send(error));	
	},
	findById(req, res) {
		return Country
			.findById(req.params.countryId)
			.then(country => {
				if(!country) {
					return res.status(404).send({
						message: 'Country not found',
					});
				}
				return res.status(200).send(country)
			})
			.catch(error => res.status(400).send(error));
	},
};