const { expect } = require('chai')

const {
  sequelize,
  dataTypes,
  checkPropertyExists
} = require('sequelize-test-helpers')

const FoodTypeModel = require('../server/models').FoodType

describe('server/models/foodType', () => {
  const FoodType = new FoodTypeModel(sequelize, dataTypes)

  context('properties', () => {
    ;[
      'id',
      'name',
      'description'
    ].forEach(checkPropertyExists(FoodType))
  })
})