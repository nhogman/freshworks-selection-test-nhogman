const { expect } = require('chai')

const {
  sequelize,
  dataTypes,
  checkPropertyExists
} = require('sequelize-test-helpers')

const QuantityTypeModel = require('../server/models').QuantityType

describe('server/models/quantityType', () => {
  const QuantityType = new QuantityTypeModel(sequelize, dataTypes)

  context('properties', () => {
    ;[
      'id',
      'unit',
      'description'
    ].forEach(checkPropertyExists(QuantityType))
  })
})