const { expect } = require('chai')

const {
  sequelize,
  dataTypes,
  checkPropertyExists
} = require('sequelize-test-helpers')

const ParkModel = require('../server/models').Park

describe('server/models/park', () => {
  const Park = new ParkModel(sequelize, dataTypes)

  context('properties', () => {
    ;[
      'id',
      'cityId',
      'name'
    ].forEach(checkPropertyExists(Park))
  })
})