const { expect } = require('chai')

const {
  sequelize,
  dataTypes,
  checkPropertyExists
} = require('sequelize-test-helpers')

const CityModel = require('../server/models').City

describe('server/models/city', () => {
  const City = new CityModel(sequelize, dataTypes)
  context('properties', () => {
    ;[
      'id',
      'name',
      'countryId'
    ].forEach(checkPropertyExists(City))
  })
})