const { expect } = require('chai')

const {
  sequelize,
  dataTypes,
  checkPropertyExists
} = require('sequelize-test-helpers')

const CountryModel = require('../server/models').Country

describe('server/models/country', () => {
  const Country = new CountryModel(sequelize, dataTypes)

  context('properties', () => {
    ;[
      'id',
      'countryCode',
      'name'
    ].forEach(checkPropertyExists(Country))
  })

  // context('associations', () => {
  //   const Company = 'some dummy company'

  //   before(() => {
  //     Country.associate({ Company })
  //   })

  //   it('defined a belongsTo association with Company', () => {
  //     expect(User.belongsTo).to.have.been.calledWith(Company)
  //   })
  // })

  
})