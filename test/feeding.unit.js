const { expect } = require('chai')

const {
  sequelize,
  dataTypes,
  checkPropertyExists
} = require('sequelize-test-helpers')

const FeedingModel = require('../server/models').Feeding

describe('server/models/feeding', () => {
  const Feeding = new FeedingModel(sequelize, dataTypes)

  context('properties', () => {
    ;[
      'id',
      'feedingTime',
      'parkId',
      'quantity',
      'quantityTypeId',
      'foodTypeId',
      'numberOfDucks'
    ].forEach(checkPropertyExists(Feeding))
  })
})