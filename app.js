const express = require('express');
var path = require('path');
const logger = require('morgan');

const bodyParser = require('body-parser');

const app = express();

app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

require('./server/routes')(app);

app.get('/', (req, res) => {
	res.sendFile(path.resolve(__dirname, 'client/dist/index.html'));
});

app.get('/main.js', (req, res) => {
	res.sendFile(path.resolve(__dirname, 'client/dist/main.js'));
});

module.exports = app;