import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/FeedingApp.jsx';

ReactDOM.render(<App />, document.getElementById('app'));