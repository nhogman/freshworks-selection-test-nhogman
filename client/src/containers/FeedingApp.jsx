import React, { Component } from 'react';
import Select from 'react-select';
import AsynSelect from 'react-select/lib/Async';
import CreatableSelect from 'react-select/lib/AsyncCreatable';
import DateTime from 'react-datetime';

import { Form, Text } from 'informed';

import FetchHelper from '../util/fetchHelper';


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      country: null,
      city: null,
      park: null,
      foodType: null,
      quantityType: null,
      quantity: 0,
      quantityTypes: [],
      numberOfDucks: 0,
      feedingTime: new Date(),
    };
    this.unitOptions = FetchHelper.get('/api/quantityTypes')
      .then((unitTypes) => {
        const quantityTypes = [];
        unitTypes.forEach((unitType) => {
          quantityTypes.push({ label: unitType.unit, id: unitType.id, value: unitType.unit });
        });
        this.setState({
          quantityTypes,
        });
      });

    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleCityChange = this.handleCityChange.bind(this);
    this.handleParkChange = this.handleParkChange.bind(this);
    this.handleFoodTypeChange = this.handleFoodTypeChange.bind(this);
    this.handleQuantityTypeChange = this.handleQuantityTypeChange.bind(this);
    this.handleQuantityChange = this.handleQuantityChange.bind(this);
    this.handleNumberOfDucksChange = this.handleNumberOfDucksChange.bind(this);
    this.handleFeedingTimeChange = this.handleFeedingTimeChange.bind(this);

    this.handleCityCreate = this.handleCityCreate.bind(this);
    this.handleParkCreate = this.handleParkCreate.bind(this);
    this.handleFoodTypeCreate = this.handleFoodTypeCreate.bind(this);

    this.handleSubmitFeeding = this.handleSubmitFeeding.bind(this);
    this.validateSubmit = this.validateSubmit.bind(this);
  }

    getCountryOptions = () => FetchHelper.get('/api/countries')
      .then((countries) => {
        const results = [];
        countries.forEach((country) => {
          results.push({ label: `${country.name} (${country.countryCode})`, id: country.id, value: country.name });
        });
        return results;
      });

    getCityOptions = () => FetchHelper.get(`/api/countries/${this.state.country.id}/cities`)
      .then((cities) => {
        const results = [];
        cities.forEach((city) => {
          results.push({ label: city.name, value: city.name, id: city.id });
        });
        return results;
      });

    getParkOptions = () => FetchHelper.get(`/api/cities/${this.state.city.id}/parks`)
      .then((parks) => {
        const results = [];
        parks.forEach((park) => {
          results.push({ label: park.name, value: park.name, id: park.id });
        });
        return results;
      });

    getFoodTypeOptions = () => FetchHelper.get('/api/foodTypes')
      .then((foodTypes) => {
        const results = [];
        foodTypes.forEach((foodType) => {
          results.push({ label: foodType.name, value: foodType.name, id: foodType.id });
        });
        return results;
      });

    handleCityCreate = (cityName) => {
      const { country } = this.state;
      FetchHelper.post(`/api/countries/${country.id}/cities`, { name: cityName })
        .then((city) => {
          const newCity = {
            value: city.name,
            label: city.name,
            id: city.id,
          };
          this.setState({
            city: newCity,
          });
        });
    }

    handleParkCreate = (parkName) => {
      const { city } = this.state;
      FetchHelper.post(`/api/cities/${city.id}/parks`, { name: parkName })
        .then((park) => {
          const newPark = {
            value: park.name,
            label: park.name,
            id: park.id,
          };
          this.setState({
            park: newPark,
          });
        });
    }

    handleFoodTypeCreate = foodTypeName => FetchHelper.post('/api/foodTypes', { name: foodTypeName })
      .then((foodType) => {
        const newFoodType = {
          value: foodType.name,
          label: foodType.name,
          id: foodType.id,
        };
        this.setState({
          foodType: newFoodType,
        });
      });

    handleCountryChange(val) {
      this.setState({
        country: val,
      });
    }

    handleCityChange(val) {
      this.setState({
        city: val,
      });
    }

    handleParkChange(val) {
      this.setState({
        park: val,
      });
    }

    handleFoodTypeChange(val) {
      this.setState({
        foodType: val,
      });
    }

    handleQuantityTypeChange(val) {
      this.setState({
        quantityType: val,
      });
    }

    handleQuantityChange(e) {
      this.setState({
        quantity: e.target.value,
      });
    }

    handleNumberOfDucksChange(e) {
      this.setState({
        numberOfDucks: e.target.value,
      });
    }

    handleFeedingTimeChange(val) {
      this.setState({
        feedingTime: val,
      });
    }

    handleSubmitFeeding() {
      if (this.validateSubmit()) {
        const {
          park,
          foodType,
          quantity,
          quantityType,
          numberOfDucks,
          feedingTime,
        } = this.state;
        FetchHelper.post(`/api/parks/${park.id}/feedings`, {
          foodTypeId: foodType.id,
          quantity: parseFloat(quantity),
          quantityTypeId: quantityType.id,
          numberOfDucks: parseInt(numberOfDucks, 10),
          feedingTime: feedingTime,
        }).then(() => {
          alert('Feeding has successfully been logged :)');
        }).catch((error) => {
          alert(error);
        });
      }
    }

    validateSubmit() {
      const {
        park,
        foodType,
        quantity,
        quantityType,
        numberOfDucks,
      } = this.state;
      if (!park) {
        alert('Please select a park.');
        return false;
      }
      if (!foodType) {
        alert('Please select a food type.');
        return false;
      }
      if (quantity === 0) {
        alert('Please enter a quantity.');
        return false;
      }
      if (!quantityType) {
        alert('Please select a quantity unity type.');
        return false;
      }
      if (numberOfDucks === 0) {
        alert('Please enter the number of ducks.');
        return false;
      }
      return true;
    }

    render() {
      const {
        country,
        city,
        park,
        foodType,
        quantity,
        quantityType,
        quantityTypes,
        numberOfDucks,
        feedingTime,
      } = this.state;
      return (
        <Form id="feeding-form">
          <label htmlFor="country-selector">Country Name:</label>
          <AsynSelect
            id="country-selector"
            loadOptions={this.getCountryOptions}
            onChange={this.handleCountryChange}
            value={country}
            defaultOptions
          />
          <label htmlFor="city-selector">City Name:</label>
          <CreatableSelect
            id="city-selector"
            onChange={this.handleCityChange}
            loadOptions={this.getCityOptions}
            onCreateOption={this.handleCityCreate}
            value={city}
            isDisabled={(!country)}
          />
          <label htmlFor="park-selector">Park Name:</label>
          <CreatableSelect
            id="park-selector"
            onChange={this.handleParkChange}
            loadOptions={this.getParkOptions}
            value={park}
            onCreateOption={this.handleParkCreate}
            isDisabled={(!city)}
            defaultOptions={(city)}
          />
          <label htmlFor="food-type-selector">Food Type:</label>
          <CreatableSelect
            id="food-type-selector"
            onChange={this.handleFoodTypeChange}
            loadOptions={this.getFoodTypeOptions}
            value={foodType}
            onCreateOption={this.handleFoodTypeCreate}
            defaultOptions
          />
          <label htmlFor="quantity-number">Quantity:</label>
          <br />
          <Text
            field="name"
            id="quantity-number"
            type="number"
            step="0.001"
            value={quantity}
            onChange={this.handleQuantityChange}
          />
          <br />
          <label htmlFor="quantity-type-selector">Unit for Quantity:</label>
          <Select
            id="quantity-type-selector"
            onChange={this.handleQuantityTypeChange}
            options={quantityTypes}
            value={quantityType}
          />
          <label htmlFor="number-of-ducks">Number of Ducks:</label>
          <br />
          <Text
            field="number"
            id="number-of-ducks"
            type="number"
            step="1"
            value={numberOfDucks}
            onChange={this.handleNumberOfDucksChange}
          />
          <br />
          <DateTime
            value={feedingTime}
            onChange={this.handleFeedingTimeChange}
          />
          <button
            type="submit"
            onClick={this.handleSubmitFeeding}
          >
            Submit Feeding
          </button>
        </Form>
      );
    }
}
