const host = window.location.host;

const FetchHelper = {
  get: url => (
    fetch(`https://${host}${url}`,
      {
        method: 'GET',
        credentials: 'same-origin',
      }).then(res => res.json())
  ),
  post: (url, params) => (
    fetch(`https://${host}${url}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(params),
      }).then(res => res.json())
  ),
};

export default FetchHelper;
