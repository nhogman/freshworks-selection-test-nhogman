var webpack = require('webpack');
var path = require('path');

var parentDir = path.join(__dirname, '../');

module.exports = {
    mode: 'development',
    entry: [
        path.join(parentDir, 'index.js')
    ],
    module: {
        rules: [{
            test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },{
                test: /\.less$/,
                loaders: ["style-loader", "css-loder", "less-loader"]
            }
        ]
    },
    output: {
        PATH: path.resolve(__dirname, 'public/'),
        filename: 'bundle.js',
        publicPath: '/public/',
      },
    devServer: {
        contentBase: parentDir,
        historyApiFallback: true
    }
}